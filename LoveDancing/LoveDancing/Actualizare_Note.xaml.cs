﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Actualizare_Note : ContentPage
    {
        public Actualizare_Note()
        {
            InitializeComponent();
        }

        private void ButonActualizare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;


            if (!con.Ping())
                con.Open();

            if (EntryAn.Text == "" || EntryNota.Text == "" || EntryNr.Text == "" || EntryTip.Text == "")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
                MySqlCommand cmd = new MySqlCommand("SELECT Nr_matricol FROM elevi WHERE Nr_matricol = '" + this.EntryNr.Text + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista elevul introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    MySqlCommand cmd5 = new MySqlCommand("INSERT INTO note(Tip_examen,Nota,An_scolar,Nr_matricol) values('" + this.EntryTip.Text + "','" + this.EntryNota.Text + "','" + this.EntryAn.Text + "','" + this.EntryNr.Text + "')", con);
                    cmd5.ExecuteNonQuery();



                    DisplayAlert("Succes", "Nota a fost actualizata", "OK");
                    con.Close();

                    Navigation.PushAsync(new Admin());

                }
            }
        }
    }
}