﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
using PdfKit;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Adaugare_elev : ContentPage
    {
        public Adaugare_elev()
        {
            InitializeComponent();

        }
        private void AdaugaButton_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            MySqlCommand cmd;
            if (!con.Ping())
                con.Open();


            if (EntryCNP.Text == "" || EntryTelefon.Text =="" || EntryPCont.Text == "" || EntryNume.Text == "" || EntryPrenume.Text == "" || EntryNCont.Text == "" || EntryEmail.Text == "")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else

            {
                string CNP = EntryCNP.Text;
                string telefon = EntryTelefon.Text;

                if (CNP.Length > 13)
                    DisplayAlert("Alerta", "Ati gresit CNP-ul", "OK");
               else 
                {
                    string sub1 = CNP.Substring(1,2);
                     
                    string sub2 = CNP.Substring(0, 1);

                    string an = null;

                    if (sub2 == "5" || sub2 == "6")
                    an = "20" + sub1;
                    else
                    an = "19" + sub1;

                    int ann = Convert.ToInt32(an);
                    int ancurent = DateTime.Now.Year;
                    if (ancurent - ann < 18 && EntryReprez.Text == "")
                        DisplayAlert("Atentie", "Are sub 18 ani! Adauga reprezentant", "OK");
                    else
                    {
                        if(this.EntryReprez.Text != "")
                            cmd = new MySqlCommand("INSERT INTO elevi(Nume,Prenume,CNP,Telefon,Nume_cont,Parola_Cont,Email,Cod_reprez) values('" + this.EntryNume.Text + "','" + this.EntryPrenume.Text + "','" + CNP + "','" + telefon + "','" + this.EntryNCont.Text + "','" + this.EntryPCont.Text + "','" + this.EntryEmail.Text+ "','" + this.EntryReprez.Text + "')", con);
                        else
                            cmd = new MySqlCommand("INSERT INTO elevi(Nume,Prenume,CNP,Telefon,Nume_cont,Parola_Cont,Email) values('" + this.EntryNume.Text + "','" + this.EntryPrenume.Text + "','" + CNP + "','" + telefon + "','" + this.EntryNCont.Text + "','" + this.EntryPCont.Text + "','" + this.EntryEmail.Text + "'"+ ")", con);

                        cmd.ExecuteNonQuery();

                        cmd = new MySqlCommand($"Select Nr_matricol From elevi WHERE Nume='{this.EntryNume.Text }' AND Prenume='{this.EntryPrenume.Text}'",con);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        string id = reader[0].ToString();
                        reader.Close();

                        cmd = new MySqlCommand("INSERT INTO Prezenta(Nr_prezente,Nr_absente,An_scolar,Nr_matricol) values('0','0',year(now()),'" + id + "')",con);
                        cmd.ExecuteNonQuery();
                        DisplayAlert("Succes", "Elev inregistrat cu succes", "OK");
                        con.Close();

                        Navigation.PushAsync(new Admin());

                    }



                }









            } }
    }
}