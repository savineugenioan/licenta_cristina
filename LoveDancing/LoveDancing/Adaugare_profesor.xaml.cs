﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Reflection;
using System.IO;
using Xamarin.Forms.PlatformConfiguration;
using CoreText;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Adaugare_profesor : ContentPage
    {
        public Adaugare_profesor()
        {
            InitializeComponent();
            selectedImage.Source = Clase_globale.Poza("nouser.jpg");
        }

        public byte[] imageToByte(MediaFile selectedImageFile)
        {
            using (var memoryStream = new MemoryStream())
            {
                selectedImageFile.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
        public ImageSource byteArrayToImage(byte[] byteArrayIn)
        {
            //return ImageSource.FromStream(() => byteArrayIn);
            Image image = new Image();
            var x = ImageSource.FromStream(() => new MemoryStream(byteArrayIn));
            return x;
        }
        async void Handle_Clicked(object sender, EventArgs e)
        {
           await CrossMedia.Current.Initialize();
            if (!CrossMedia.IsSupported)
                await DisplayAlert("e", "NU", "ok");
            var mediaOptions = new PickMediaOptions()
            {
                PhotoSize = PhotoSize.Medium
            };
            var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);
            if (selectedImageFile == null)
            {
                selectedImage.Source = Clase_globale.Poza("nouser.jpg");
                Clase_globale.image = null;
            }
            else
            {
                Clase_globale.image = selectedImageFile;
                selectedImage.Source = byteArrayToImage(imageToByte(selectedImageFile));
            }

        }

        private void ButonAdaugare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();

            if (EntryTelefon.Text == "" || EntryNume.Text == "" || EntryPrenume.Text == "" || EntryCNP.Text == "" || EntryAdresa.Text == "" || EntryLoc.Text == "" || EntryNumec.Text == "" || EntryParolaC.Text == "" )
                DisplayAlert("Alerta", "Completati toate campurile", "OK");

            else
            {
                string CNP = EntryCNP.Text;
                string telefon = EntryTelefon.Text;
                if (CNP.Length > 13)
                    DisplayAlert("Alerta", "Ati gresit CNP-ul", "OK");
                else
                {
                    MySqlCommand cmd;
                    if (Clase_globale.image != null)
                    {
                        cmd = new MySqlCommand("INSERT INTO profesori(Nume,Prenume,CNP,Telefon,Localitate,Strada_nr,Parola_cont,Nume_cont,poza) values('" + this.EntryNume.Text + "','" + this.EntryPrenume.Text + "','" + CNP + "','" + telefon + "','" + this.EntryLoc.Text + "','" + this.EntryAdresa.Text + "','" + this.EntryParolaC.Text + "','" + this.EntryNumec.Text + "',@userImage" + ")", con);
                        var userImage = imageToByte(Clase_globale.image);
                        var paramUserImage = new MySqlParameter("@userImage", MySqlDbType.Blob, userImage.Length);
                        paramUserImage.Value = userImage;
                        cmd.Parameters.Add(paramUserImage);
                        Clase_globale.image = null;
                    }
                    else
                        cmd = new MySqlCommand("INSERT INTO profesori(Nume,Prenume,CNP,Telefon,Localitate,Strada_nr,Parola_cont,Nume_cont) values('" + this.EntryNume.Text + "','" + this.EntryPrenume.Text + "','" + CNP + "','" + telefon + "','" + this.EntryLoc.Text + "','" + this.EntryAdresa.Text + "','" + this.EntryParolaC.Text + "','" + this.EntryNumec.Text + "')", con);
                    cmd.ExecuteNonQuery();
                    

                    DisplayAlert("Succes", "Profesor inregistrat cu succes", "OK");
                    con.Close();

                    Navigation.PushAsync(new Admin());




                }
            }
        }
    }
}