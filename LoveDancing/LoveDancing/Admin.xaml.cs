﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Admin : ContentPage
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void ActNoteButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Actualizare_Note());
        }

        private void RepartizareProf_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Creare_grupa());
        }

        private void RepartizareElev_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Repartizati_elev_grupa());
        }

        private void CuprinsCreare_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Cuprins_creare());
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MainPage());
        }

        private void ButtonNou_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Creare_noutati());
        }

        private void CreareRapoarte_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Rapoarte());
        }

        private void Orar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CreareOrar());
        }
    }
}