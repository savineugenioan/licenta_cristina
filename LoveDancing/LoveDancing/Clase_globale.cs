﻿using MySql.Data.MySqlClient;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace LoveDancing
{
    class Clase_globale
    {
        public static MySqlConnection con;
        public static MediaFile image = null;
        public static ImageSource Poza(string denumire)
        {
            var imageSource = ImageSource.FromResource("LoveDancing.Resurse.Imagini." + denumire);
            return imageSource;
        }
        public static bool Major(string CNP)
        {
            string sub1 = CNP.Substring(1, 2);

            string sub2 = CNP.Substring(0, 1);

            string an = null;

            if (sub2 == "5" || sub2 == "6")
                an = "20" + sub1;
            else
                an = "19" + sub1;

            int ann = Convert.ToInt32(an);
            int ancurent = DateTime.Now.Year;
            if (ancurent - ann < 18)
                return false;
            else
                return true;
        }
        public static string NumeContG;
        public static string ParolaContG;
        public static int GrupaSelect;
    }
}
