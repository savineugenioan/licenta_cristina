﻿using MySql.Data.MySqlClient;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreareOrar : ContentPage
    {
        public CreareOrar()
        {
            InitializeComponent();
            selectedImage.Source = Clase_globale.Poza("nouser.jpg");
        }
        public byte[] imageToByte(MediaFile selectedImageFile)
        {
            using (var memoryStream = new MemoryStream())
            {
                selectedImageFile.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
        public ImageSource byteArrayToImage(byte[] byteArrayIn)
        {
            //return ImageSource.FromStream(() => byteArrayIn);
            Image image = new Image();
            var x = ImageSource.FromStream(() => new MemoryStream(byteArrayIn));
            return x;
        }

        private void ButtonBaza_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();

            MySqlCommand cmd;
            if (Clase_globale.image != null && EntryOrar.Text!= null && EntryOrar.Text!="")
            {
                cmd = new MySqlCommand($"Select Nr_sediu FROM Orar Where Nr_sediu={EntryOrar.Text}",con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    cmd = new MySqlCommand($"UPDATE Orar SET Orar=@userImage WHERE Nr_sediu={EntryOrar.Text};", con);
                    var userImage = imageToByte(Clase_globale.image);
                    var paramUserImage = new MySqlParameter("@userImage", MySqlDbType.Blob, userImage.Length);
                    paramUserImage.Value = userImage;
                    cmd.Parameters.Add(paramUserImage);
                    Clase_globale.image = null;
                }
                else
                {
                    cmd = new MySqlCommand($"INSERT INTO Orar(Nr_sediu,Orar) values({EntryOrar.Text},@userImage);", con);
                    var userImage = imageToByte(Clase_globale.image);
                    var paramUserImage = new MySqlParameter("@userImage", MySqlDbType.Blob, userImage.Length);
                    paramUserImage.Value = userImage;
                    cmd.Parameters.Add(paramUserImage);
                    Clase_globale.image = null;
                }
                reader.Close();

                cmd.ExecuteNonQuery();

                DisplayAlert("Succes", "Orar inregistrat cu succes", "OK");
                con.Close();
                Navigation.PushAsync(new Admin());
            }
            else
            {
                DisplayAlert("Eroare", "Incarcati o poza si numar sediu", "OK");
            }
           
        }

        async private void Select_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.IsSupported)
                await DisplayAlert("e", "NU", "ok");
            var mediaOptions = new PickMediaOptions()
            {
                PhotoSize = PhotoSize.Medium
            };
            var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);
            if (selectedImageFile == null)
            {
                selectedImage.Source = Clase_globale.Poza("nouser.jpg");
                Clase_globale.image = null;
            }
            else
            {
                Clase_globale.image = selectedImageFile;
                selectedImage.Source = byteArrayToImage(imageToByte(selectedImageFile));
            }
        }
    }
}