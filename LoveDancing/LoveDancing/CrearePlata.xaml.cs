﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearePlata : ContentPage
    {
        public CrearePlata()
        {
            InitializeComponent();
        }

        private void ButonAdauga_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            if (EntryNr.Text == "" || EntrySuma.Text == "")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
                MySqlCommand cmd = new MySqlCommand("SELECT Nr_abonament FROM abonament WHERE Nr_abonament = '" + this.EntryNr.Text + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista abonamentul  introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    int suma = Int32.Parse(EntrySuma.Text);

                    MySqlCommand cmd1 = new MySqlCommand("INSERT INTO plati(Suma_platita,Nr_abonament) values('" + suma + "','" + this.EntryNr.Text +  "')", con);
                    cmd1.ExecuteNonQuery();
                    

                    MySqlCommand cmdupdate = new MySqlCommand($"UPDATE abonament SET suma_platita=suma_platita+{suma} ,suma_ramasa=suma_Totala-suma_platita  WHERE  Nr_abonament = '" + this.EntryNr.Text + "' ", con);
                    cmdupdate.ExecuteNonQuery();

                   DisplayAlert("Succes", "Plata creata  cu succes", "OK");
                    con.Close();

                    Navigation.PushAsync(new Admin());
                }
            }

        }
    }
}