﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Creare_abonament : ContentPage
    {
        public Creare_abonament()
        {
            InitializeComponent();
        }

        private void ButonAdauga_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            if (EntryNr.Text == "" || EntryAbonament.Text == "" || EntrySuma.Text==""  )
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
                MySqlCommand cmd = new MySqlCommand("SELECT Nr_matricol FROM Elevi WHERE Nr_matricol = '" + this.EntryNr.Text + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista elevul introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();

                    int nr = Int32.Parse(EntryNr.Text);
                    int a = 0;


                    string theDate = DataAleasa.Date.ToString();

                     string   data = DataAleasa.Date.ToString("yyyy-mm-dd");
                    /*data.Format = "yyyy-MM-dd";*/
                    //data.Date;

                    MySqlCommand cmd1 = new MySqlCommand("INSERT INTO abonament(Tip_abonament,Data_inceput,Nr_matricol,Suma_Totala,suma_platita,suma_ramasa,restanta) values('" + this.EntryAbonament.Text + "','" + data + "','" + nr + "','"+ this.EntrySuma.Text + "','"+ a +"','"+ a + "','" + a + "')", con);
                    cmd1.ExecuteNonQuery();



                    DisplayAlert("Succes", "Abonament creat  cu succes", "OK");
                    con.Close();

                    Navigation.PushAsync(new Admin());
                }
            }



        }
    }
}