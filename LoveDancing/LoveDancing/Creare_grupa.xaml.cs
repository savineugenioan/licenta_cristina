﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Creare_grupa : ContentPage
    {
        public Creare_grupa()
        {
            InitializeComponent();
        }

        private void ButonCreare_Clicked(object sender, EventArgs e)
        {

            MySqlConnection con = Clase_globale.con ;
            if(!con.Ping())
            con.Open();

            if (EntryAn.Text == "" || EntrySediu.Text == "" || EntryProf.Text == "" || EntryPProf.Text == "" || EntryStil.Text == "" || EntryGrupa.Text =="")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
               
                MySqlCommand cmd = new MySqlCommand("SELECT Nr_sediu FROM Sediu WHERE Nr_sediu = '" + this.EntrySediu.Text + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista sediul introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    MySqlCommand cmd1 = new MySqlCommand("SELECT Nume, Prenume FROM Profesori WHERE Nume = '" + this.EntryProf.Text + "' AND Prenume='" + this.EntryPProf.Text + "'", con);
                    MySqlDataReader reader1 = cmd1.ExecuteReader();
                    reader1.Read();
                    if (reader1.HasRows == false)
                    {
                        DisplayAlert("Alerta", "Nu exista profesorul  introdus", "OK");
                        reader1.Close();
                    }
                    else
                    {
                        reader1.Close();
                        MySqlCommand cmd2 = new MySqlCommand("SELECT Nume FROM stiluri_dans WHERE Nume = '" + this.EntryStil.Text + "' ", con);
                        MySqlDataReader reader2 = cmd2.ExecuteReader();
                        reader2.Read();
                        if (reader2.HasRows == false)
                        {
                            DisplayAlert("Alerta", "Nu exista stilul de dans introdus", "OK");
                            reader2.Close();
                        }
                        else
                        {
                            reader2.Close();
                            MySqlCommand cmdprof = new MySqlCommand("SELECT MarcaProf FROM Profesori WHERE Nume = '" + this.EntryProf.Text + "' AND Prenume='" + this.EntryPProf.Text + "'", con);
                            MySqlDataReader readerprof = cmdprof.ExecuteReader();
                            readerprof.Read();
                            string Profesori = readerprof[0].ToString();
                            readerprof.Close();

                            MySqlCommand cmdstil = new MySqlCommand("SELECT Nr_stil FROM stiluri_dans WHERE Nume = '" + this.EntryStil.Text + "' ", con);
                            MySqlDataReader readerstil = cmdstil.ExecuteReader();
                            readerstil.Read();
                            string stil = readerstil[0].ToString();

                            readerstil.Close();

                            MySqlCommand cmd5 = new MySqlCommand("INSERT INTO grupe(Nr_grupe,An_scolar,Nr_sediu,MarcaProf,Nr_stil) values('" + this.EntryGrupa.Text + "','" + this.EntryAn.Text + "','" + this.EntrySediu.Text + "','" + Profesori + "','" + stil + "')", con);
                            cmd5.ExecuteNonQuery();



                            DisplayAlert("Succes", "Grupa creata  cu succes", "OK");
                            con.Close();

                            Navigation.PushAsync(new Admin());
                        }
                    }
                }

            }
        }
    }
}