﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Creare_noutati : ContentPage
    {
        public Creare_noutati()
        {
            InitializeComponent();
        }

        private void ButonCreare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();

            if (EntryNou.Text == "" )
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO Noutati(noutate) values('" + this.EntryNou.Text + "')", con);
                cmd.ExecuteNonQuery();



                DisplayAlert("Succes", "Noutatea a fost salvata", "OK");
                con.Close();

                Navigation.PushAsync(new Admin());
            }

        }
    }
}