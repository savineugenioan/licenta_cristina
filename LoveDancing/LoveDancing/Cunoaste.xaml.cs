﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cunoaste_ne : ContentPage
    {
        public Cunoaste_ne()
        {
            InitializeComponent();
        }
        private void Profesori_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Profesori());
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Harti1());
        }

        private void Stiluri_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Stiluri());
        }

        private void Review_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Review());
        }

        private void Contact_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Contact());
        }
    }
}