﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cuprins_creare : ContentPage
    {
        public Cuprins_creare()
        {
            InitializeComponent();
        }

        private void AdaugareReprButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Adaugare_reprez());
        }

        private void AdaugareElevButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Adaugare_elev());
        }

        private void AdaugareProfButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Adaugare_profesor());
        }

        private void CreareGrupaButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Creare_grupa());
        }

        private void CreareAboanmentButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Creare_abonament());
        }

        private void CrearePlataButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CrearePlata());
        }
    }
}