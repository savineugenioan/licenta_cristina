﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Harti1 : ContentPage
    {
        public Harti1()
        {
            InitializeComponent();
        }

        private  async void ButtonOpenCoords_Clicked(object sender, EventArgs e)
        {
             await Map.OpenAsync(44.4366848, 26.0374528, new MapLaunchOptions
            {
                
                NavigationMode = NavigationMode.None

            });
        }

        private  async void ButtonOpenCoords1_Clicked(object sender, EventArgs es)
        {
            await Map.OpenAsync(44.4288906, 26.0487114, new MapLaunchOptions
            {
               
                NavigationMode = NavigationMode.None

            });
        }

        private async void ButtonOpenCoords2_Clicked(object sender, EventArgs es)
        {
            await Map.OpenAsync(44.43067, 26.0731133, new MapLaunchOptions
            {
                //Name = EntryName.Text,
                NavigationMode = NavigationMode.None

            });
        }

        private async void ButtonOpenCoords3_Clicked(object sender, EventArgs es)
        {
            await Map.OpenAsync(45.1758233, 28.8023576, new MapLaunchOptions
            {
                //Name = EntryName.Text,
                NavigationMode = NavigationMode.None

            });
        }
        private async void ButtonOpenCoords4_Clicked(object sender, EventArgs es)
        {
            await Map.OpenAsync(44.180667, 28.6287473, new MapLaunchOptions
            {
                //Name = EntryName.Text,
                NavigationMode = NavigationMode.None

            });
        }


    }
}