﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class Intra : ContentPage
    {
        public Intra()
        {
            InitializeComponent();
        }

        private void ButonLogare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;


            if (!con.Ping())
                con.Open();

            if (EntryNume.Text == "" || EntryParola.Text == "")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {

                var p = PickerName.SelectedItem;
                string poate = p.ToString();
                if (poate == "Elev")
                {
                    MySqlCommand cmd = new MySqlCommand("SELECT Nume_cont,Parola_cont FROM elevi WHERE Nume_cont = '" + this.EntryNume.Text + "' AND Parola_cont='" + this.EntryParola.Text + "' ", con);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows == false)
                    {
                        DisplayAlert("Alerta", "Ai introdus gresit numarul de cont", "OK");

                    }
                    else
                    {
                        Clase_globale.NumeContG = this.EntryNume.Text;
                        Clase_globale.ParolaContG = this.EntryParola.Text;
                        reader.Close();
                        Navigation.PushAsync(new CuprinsElev());
                       
                    }
                    reader.Close();
                }
                else
                {
                    if (poate == "Profesor")
                    {
                        MySqlCommand cmdP = new MySqlCommand("SELECT Nume_cont,Parola_cont FROM profesori WHERE Nume_cont = '" + this.EntryNume.Text + "' AND Parola_cont='" + this.EntryParola.Text + "' ", con);
                        MySqlDataReader readerP = cmdP.ExecuteReader();
                        readerP.Read();
                        if (readerP.HasRows == false)
                        {
                            DisplayAlert("Alerta", "Ai introdus gresit contul sau parola", "OK");
                           
                        }
                        else
                        {
                            Clase_globale.NumeContG = this.EntryNume.Text;
                            Clase_globale.ParolaContG = this.EntryParola.Text;
                            readerP.Close();
                            Navigation.PushAsync(new CuprinsProf());
                        }
                        readerP.Close();
                    }
                    else
                        if (poate == "Admin")
                    {
                        MySqlCommand cmdP = new MySqlCommand("SELECT Nume_cont,Parola_cont FROM admin WHERE Nume_cont = '" + this.EntryNume.Text + "' AND Parola_cont='" + this.EntryParola.Text + "' ", con);
                        MySqlDataReader readerP = cmdP.ExecuteReader();
                        readerP.Read();
                        if (readerP.HasRows == false)
                        {
                            DisplayAlert("Alerta", "Ai introdus gresit contul sau parola", "OK");

                        }
                        else
                        {
                            Clase_globale.NumeContG = this.EntryNume.Text;
                            Clase_globale.ParolaContG = this.EntryParola.Text;
                            readerP.Close();
                            Navigation.PushAsync(new Admin());
                        }
                        readerP.Close();

                    }
                        

                }

            }
        }
    }
}