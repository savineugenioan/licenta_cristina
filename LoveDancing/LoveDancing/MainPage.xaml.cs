﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    
  
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            string conexiune = "Server = 192.168.100.3;Port=3306; Database = licenta; Uid = root; Pwd =1234;";
            Clase_globale.con = new MySqlConnection(conexiune);
            Clase_globale.con.Open();
            InitializeComponent();

          
        }
        private void CunoasteButton_Clicked(object sender ,EventArgs e)
        {
            Navigation.PushAsync(new Cunoaste_ne());
        }

        private void IntraButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Intra());
        }
    }
}
