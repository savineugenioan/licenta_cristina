﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Note : ContentPage

    {
        string NumeG = Clase_globale.NumeContG;
        string ParolaG = Clase_globale.ParolaContG;

        public Note()
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            InitializeComponent();
            string sql = $"SELECT n.Tip_examen, n.nota FROM elevi e INNER JOIN note n ON e.Nr_matricol=n.Nr_matricol  WHERE Nume_cont='{NumeG}';";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader readerP = cmd.ExecuteReader();
            while (readerP.Read())
            { 
                if (readerP[0].ToString() =="initial")
                {
                    EntryInit.Text = readerP[1].ToString();
                }
                else
                    EntryFinal.Text = readerP[1].ToString();

            }
            readerP.Close();
        }
    }
}