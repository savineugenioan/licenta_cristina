﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
using Syncfusion.Pdf.Interactive;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Noutati : ContentPage
    {
        public Noutati()
        {
         
            InitializeComponent();
            var layout = this.FindByName<StackLayout>("noutati");
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string sql = "SELECT * FROM noutati;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var MyEntry = new Editor { Text = reader[1].ToString() + Environment.NewLine + Environment.NewLine  };
               
                MyEntry.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
                layout.Children.Add(MyEntry);
            }
            reader.Close();
        }
    }
}