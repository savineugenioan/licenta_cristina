﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Orar : ContentPage
    {
        public Orar()
        {
            InitializeComponent();

            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string sql = "Select Nr_Sediu, Orar FROM orar;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            var layout = this.FindByName<StackLayout>("orar");
            while (reader.Read())
            {
                var stack = new StackLayout();

                var MyEntry = new Label { Text =reader[0].ToString(), FontSize = 20 };
                MyEntry.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
                MyEntry.HorizontalOptions = LayoutOptions.Center;

                var orar = new Image { Source = byteArrayToImage((byte[])reader[1]), WidthRequest = 400, HeightRequest = 300 ,HorizontalOptions=LayoutOptions.Center };

                stack.Children.Add(MyEntry);
                stack.Children.Add(orar);
                layout.Children.Add(stack);
            }
            reader.Close();
        }

        public ImageSource byteArrayToImage(byte[] byteArrayIn)
        {
            //return ImageSource.FromStream(() => byteArrayIn);
            Image image = new Image();
            var x = ImageSource.FromStream(() => new MemoryStream(byteArrayIn));
            return x;
        }
    }
}