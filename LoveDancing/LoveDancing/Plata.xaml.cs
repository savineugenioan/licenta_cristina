﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Plata : ContentPage
    {
        string NumeG = Clase_globale.NumeContG;
        string ParolaG = Clase_globale.ParolaContG;
        public Plata()
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            InitializeComponent();

            string sql = $"SELECT a.suma_platita, a.suma_ramasa, a.restanta  FROM elevi e INNER JOIN abonament a ON e.Nr_matricol=a.Nr_matricol  WHERE Nume_cont='{NumeG}';";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader readerP = cmd.ExecuteReader();
            while (readerP.Read())
            {
                EntryPlatit.Text = readerP[0].ToString();
                Entrydeplata.Text = readerP[1].ToString();
                EntryRestanta.Text = readerP[2].ToString();
            }
            readerP.Close();
        }
    }
}