﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
using CoreVideo;
using System.Globalization;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Prezenta : ContentPage
    {
        StackLayout layout;
        Dictionary<string, int> elevi;
        public Prezenta()
        {
            InitializeComponent();
            elevi = new Dictionary<string, int>();
            layout = this.FindByName<StackLayout>("prezenta");
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            int grupSelectata = Clase_globale.GrupaSelect;
            MySqlCommand cmd = new MySqlCommand("SELECT e.Nr_Matricol,e.Nume, e.Prenume FROM elevi e INNER JOIN elevi_grupe g ON g.Nr_matricol=e.Nr_matricol WHERE Nr_grupe = '" + grupSelectata + "'", con);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var stack = new StackLayout();
                stack.Orientation = StackOrientation.Horizontal;
                var MyEntry = new Label { Text= Environment.NewLine+ reader[1].ToString() +" "+reader[2].ToString(),FontSize=20 };
                MyEntry.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
                var check = new CheckBox { Margin = 40, };
                check.HorizontalOptions = LayoutOptions.EndAndExpand;
                stack.VerticalOptions = LayoutOptions.Start;
                stack.Children.Add(MyEntry);
                stack.Children.Add(check);
                elevi.Add(reader[1].ToString() + " " + reader[2].ToString(),int.Parse(reader[0].ToString()));
                layout.Children.Add(stack);
            }
            reader.Close();
        }

        private void ButonInregistrare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            MySqlCommand cmd;
            if (!con.Ping())
                con.Open();
            layout = this.FindByName<StackLayout>("prezenta");
            var search = layout.Children;
            foreach(StackLayout elem in search)
            {
                string sql;
                Label label = (Label)elem.Children[0];
                CheckBox check = (CheckBox)elem.Children[1];
                if(check.IsChecked ==true)
                    sql = $"UPDATE prezenta SET Nr_prezente=Nr_prezente+1 WHERE year(now())=An_scolar AND Nr_matricol={elevi[label.Text.ToString().Trim()]}";
                else
                    sql = $"UPDATE prezenta SET Nr_absente=Nr_absente+1 WHERE year(now())=An_scolar  AND Nr_matricol={elevi[label.Text.ToString().Trim()]}";
                cmd = new MySqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }
            DisplayAlert("Prezenta","Prezenta efectuata cu succes","OK");
            Navigation.PushAsync(new CuprinsProf());

        }
    }
}