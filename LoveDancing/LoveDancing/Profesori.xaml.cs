﻿using CoreMedia;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Markup;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profesori : ContentPage
    {
        public ImageSource byteArrayToImage(byte[] byteArrayIn)
{
            //return ImageSource.FromStream(() => byteArrayIn);
            Image image = new Image();
            var x = ImageSource.FromStream(() => new MemoryStream(byteArrayIn));
            return x;
        }
        public Profesori()
        {
            InitializeComponent();

            var layout = this.FindByName<StackLayout>("profesori");
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string sql = "SELECT * FROM Profesori;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var image = new Image();
                var x = reader[10];
                var y = x.ToString();
                if (y != "")
                { 
                    image = new Image { Source = byteArrayToImage((byte[])x), WidthRequest=200,HeightRequest=200 };
                    image.Style = (Style)Application.Current.Resources["NormalImage"];
                    image.HorizontalOptions = LayoutOptions.Center;
                    image.Margin = 40;
                }

                var nume = new Entry { Text = "Nume: " + reader[1].ToString() + Environment.NewLine + Environment.NewLine };
                nume.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
                var prenume = new Entry { Text = "Prenume: " + reader[2].ToString() + Environment.NewLine + Environment.NewLine };
                prenume.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
                var descriere = new Editor { Text = "Descriere: " + reader[3].ToString() + Environment.NewLine + Environment.NewLine };
                descriere.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];

                layout.Children.Add(image);
                layout.Children.Add(nume);
                layout.Children.Add(prenume);
                layout.Children.Add(descriere);
            }
            reader.Close();
        }
    }
}