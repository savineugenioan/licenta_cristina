﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilElev : ContentPage
    {
        public ProfilElev()
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string NumeG = Clase_globale.NumeContG;
            string ParolaG = Clase_globale.ParolaContG;
            InitializeComponent();

         
            string sql = $"SELECT e.Nume,e.Prenume, g.Nr_grupe , s.Nume FROM elevi e INNER JOIN elevi_grupe eg ON e.Nr_matricol=eg.Nr_matricol INNER JOIN grupe g ON eg.Nr_grupe=g.Nr_grupe INNER JOIN stiluri_dans s ON g.Nr_stil=s.Nr_stil  WHERE e.Nume_cont='{NumeG}';";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader readerP = cmd.ExecuteReader();
            while (readerP.Read())
            {
                EntryNume.Text = readerP[0].ToString();
                EntryPrenume.Text = readerP[1].ToString();
                EntryClasa.Text= readerP[2].ToString();
                EntryStil.Text = readerP[3].ToString();


            }
            readerP.Close();
        }

        private void ButonDelogare_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MainPage());
        }
    }
}