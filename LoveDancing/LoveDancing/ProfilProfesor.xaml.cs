﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilProfesor : ContentPage
    {
        string NumeG = Clase_globale.NumeContG;
        string ParolaG = Clase_globale.ParolaContG;
        public ProfilProfesor()
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
           

            InitializeComponent();
            string sql = $"SELECT * FROM Profesori WHERE Nume_cont='{NumeG}';";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader readerP = cmd.ExecuteReader();
            while (readerP.Read())
            {
                EntryNume.Text = readerP[1].ToString();
                EntryPrenume.Text = readerP[2].ToString();

            }
            readerP.Close();
        }

        private void ButonPrezenta_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            if (EntryGrupa.Text == "")
                DisplayAlert("Alerta", "Nu ati completat grupa", "OK");
            else
            {
                Clase_globale.GrupaSelect = Convert.ToInt32(EntryGrupa.Text);

                MySqlCommand cmd = new MySqlCommand("SELECT Nr_grupe FROM grupe WHERE Nr_grupe = '" + Clase_globale.GrupaSelect + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista grupa introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    Navigation.PushAsync(new Prezenta());
                }
            }
        }

        private void ButonProf_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MainPage());
        }
    }
}