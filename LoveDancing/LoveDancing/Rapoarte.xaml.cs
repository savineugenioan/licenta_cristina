﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.Pdf.Tables;
using System.IO;
using System.Reflection;
using Syncfusion.SfPdfViewer.XForms;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Rapoarte : ContentPage
    {
        MySqlConnection con;
        public Rapoarte()
        {
            InitializeComponent();
            this.con = Clase_globale.con;
        }

        private void a1_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select fg.Nr_grupe,COUNT(f.nume) FROM elevi f Inner Join elevi_grupe fg ON f.Nr_matricol = fg.Nr_matricol Group By fg.Nr_grupe;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Grupa"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Elevi"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica,25 , PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("1.pdf", "application / pdf", stream);
        }

        private void a2_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select g.Nr_sediu,COUNT(fg.Nr_matricol) FROM Grupe g Inner Join elevi_grupe fg ON g.Nr_grupe = fg.Nr_grupe Group By g.Nr_sediu;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Sediu"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Elevi"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("2.pdf", "application / pdf", stream);
        }

        private void a3_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select concat(p.Nume,' ',p.Prenume),COUNT(fg.Nr_matricol) FROM Grupe g Inner Join "+
                "elevi_grupe fg ON g.Nr_grupe = fg.Nr_grupe Inner Join  Profesori p ON g.MarcaProf = p.MarcaProf Group By g.Nr_sediu;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Profesor"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Elevi"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("3.pdf", "application / pdf", stream);
        }

        private void a4_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select concat(e.Nume,' ',e.Prenume),SUM(p.nr_absente) FROM Elevi e Inner Join Prezenta p ON e.Nr_Matricol = p.Nr_Matricol Group By e.Nume;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Elev"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. absente"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("4.pdf", "application / pdf", stream);
        }

        private void a5_Clicked_1(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select fg.Nr_Grupe,SUM(p.nr_absente) FROM Elevi e Inner Join Prezenta p ON e.Nr_Matricol = p.Nr_Matricol " +
                        "Inner Join Elevi_Grupe fg ON e.Nr_Matricol = fg.Nr_Matricol Group By fg.Nr_Grupe; ";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Grupa"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. absente"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("5.pdf", "application / pdf", stream);
        }

        private void a6_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select concat(e.Nume,' ',e.Prenume),e.CNP,s.Nume FROM Elevi e Inner Join Elevi_Grupe fg ON e.Nr_Matricol = fg.Nr_Matricol"+
                            " Inner Join Grupe g ON g.Nr_Grupe = fg.Nr_Grupe Inner Join Stiluri_Dans s ON s.Nr_Stil "+
                            "= g.Nr_Stil Order By e.Nume; ";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Elevi Majori"));
            pdfLightTable.Columns.Add(new PdfColumn("Stiluri de dans"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                if(Clase_globale.Major(reader[1].ToString())==true)
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[2].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("6.pdf", "application / pdf", stream);
        }

        private void a7_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select concat(e.Nume,' ',e.Prenume),a.restanta FROM Elevi e InNER JOIN abonament a ON a.Nr_matricol = e.Nr_matricol WHERE a.restanta>0;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Elevi"));
            pdfLightTable.Columns.Add(new PdfColumn("Restanta de platit(lei)"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                    pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("7.pdf", "application / pdf", stream);
        }

        private void a8_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select concat(e.Nume,' ',e.Prenume),a.Tip_abonament FROM Elevi e InNER JOIN abonament a ON a.Nr_matricol = e.Nr_matricol ORDER BY e.Nume;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Elevi"));
            pdfLightTable.Columns.Add(new PdfColumn("Tip Abonament"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("8.pdf", "application / pdf", stream);
        }

        private void a9_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select a.Tip_abonament,Count(a.Nr_matricol) FROM abonament a Group By a.Tip_abonament;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Tip Abonament"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Elevi"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("9.pdf", "application / pdf", stream);
        }

        private void a10_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select s.Nr_sediu,COUNT(a.Nr_abonament)  FROM abonament a INNER JOIN elevi e ON e.Nr_matricol = a.Nr_matricol "+
                            "INNER Join elevi_grupe eg ON eg.Nr_matricol = e.Nr_matricol INNER JOIN grupe g ON g.Nr_grupe = eg.Nr_grupe "+
                             "INNER JOIN sediu s ON s.Nr_sediu = g.Nr_sediu Group By s.Nr_Sediu; ";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Sediu"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Abonamente"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("10.pdf", "application / pdf", stream);
        }

        private void a11_Clicked(object sender, EventArgs e)
        {
            if (!con.Ping())
                con.Open();
            string sql = "Select MONTHNAME(Data_inceput),COUNT(Nr_abonament) FROM Abonament Where Tip_abonament='lunar' AND year(Data_inceput)=year(now())  Group By month(data_inceput) ORDER BY month(data_inceput);";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            PdfDocument doc = new PdfDocument();
            PdfPage page = doc.Pages.Add();
            PdfLightTable pdfLightTable = new PdfLightTable();

            pdfLightTable.Columns.Add(new PdfColumn("Luna"));
            pdfLightTable.Columns.Add(new PdfColumn("Nr. Abonamente"));

            pdfLightTable.Style.ShowHeader = true;
            PdfCellStyle headerStyle = new PdfCellStyle();
            //Assign font
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 25, PdfFontStyle.Bold);
            //Set alignment
            headerStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set header style.
            pdfLightTable.Style.HeaderStyle = headerStyle;
            //Create new PdfCellStyle instance
            PdfCellStyle cellStyle = new PdfCellStyle();
            //Set font 
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.Helvetica, 20, PdfFontStyle.Regular);
            //Set alignment
            cellStyle.StringFormat = new PdfStringFormat(PdfTextAlignment.Center);
            //Set default cell style
            pdfLightTable.Style.DefaultStyle = cellStyle;

            while (reader.Read())
            {
                pdfLightTable.Rows.Add(new object[] { reader[0].ToString(), reader[1].ToString() });
            }
            reader.Close();
            pdfLightTable.Draw(page, new PointF(0, 0));
            //Save the document to the stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            doc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().SaveAndView("11.pdf", "application / pdf", stream);
        }
    }
}