﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Repartizati_elev_grupa : ContentPage
    {
        public Repartizati_elev_grupa()
        {
            InitializeComponent();
        }

        private void ButonRepartizare_Clicked(object sender, EventArgs e)
        {
            MySqlConnection con = Clase_globale.con;


            if (!con.Ping())
                con.Open();

            if (EntryAn.Text == "" || EntryGrupa.Text == "" || EntryNr.Text == "")
                DisplayAlert("Alerta", "Completati toate campurile", "OK");
            else
            {
                MySqlCommand cmd = new MySqlCommand("SELECT Nr_matricol FROM elevi WHERE Nr_matricol = '" + this.EntryNr.Text + "'", con);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows == false)
                {
                    DisplayAlert("Alerta", "Nu exista elevul introdus", "OK");
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    MySqlCommand cmd1 = new MySqlCommand("SELECT Nr_grupe FROM grupe  WHERE Nr_grupe = '" + this.EntryGrupa.Text + "'", con);
                    MySqlDataReader reader1 = cmd1.ExecuteReader();
                    reader1.Read();
                    if (reader1.HasRows == false)
                    {
                        DisplayAlert("Alerta", "Nu exista grupa introdus", "OK");
                        reader1.Close();
                    }
                    else
                    {
                        reader1.Close();

                        MySqlCommand cmd5 = new MySqlCommand("INSERT INTO elevi_grupe(Nr_matricol,Nr_grupe) values('" + this.EntryNr.Text + "','" + this.EntryGrupa.Text + "')", con);
                        cmd5.ExecuteNonQuery();



                        DisplayAlert("Succes", "Elevul a fost repartizat", "OK");
                        con.Close();

                        Navigation.PushAsync(new Admin());

                    }

                }
            }
        }
    }
}