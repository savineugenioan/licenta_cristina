﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Review : ContentPage
    {
        public Review()
        {
            InitializeComponent();
            var layout = this.FindByName<StackLayout>("reviews");
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string sql = "SELECT * FROM Reviews;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var MyEntry = new Editor { Text = reader[1].ToString() + Environment.NewLine};
                MyEntry.Style = (Style)Application.Current.Resources["EntryRevReadOnly"];
                MyEntry.BackgroundColor = Xamarin.Forms.Color.WhiteSmoke;
                layout.Children.Add(MyEntry);
            }
            reader.Close();
        }
    }
}