﻿using CoreMotion;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveDancing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Stiluri : ContentPage
    {
        public Stiluri()
        {
            InitializeComponent();
            var layout = this.FindByName<StackLayout>("stiluri");
            MySqlConnection con = Clase_globale.con;
            if (!con.Ping())
                con.Open();
            string sql = "SELECT * FROM STILURI_DANS;";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
              var MyEntry = new Editor { Text = "Nume Dans: "+reader[1].ToString() + Environment.NewLine + Environment.NewLine };
                MyEntry.Text += "Descriere:" + reader[2].ToString() + Environment.NewLine;
                MyEntry.Style = (Style)Application.Current.Resources["EntryProfiReadOnly"];
              layout.Children.Add(MyEntry);
            }
            reader.Close();

        }
    }
}